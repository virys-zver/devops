#!/bin/bash

# -p PASSWORD
# -d DOMAIN

while getopts "p:d:" option; do
   case $option in
      p)
        PASSWORD=$OPTARG;;
      d)
        DOMAIN=$OPTARG;;
     \?)
         echo "Error: Invalid option"
         exit;;
   esac
done

if [ -z "$PASSWORD" ] || [ -z "$DOMAIN" ]; then
    echo "Usage: $0 [-p PASSWORD] [-d DOMAIN]";
    echo "Example: $0 -p password -d domain.com";
    exit 1;
fi

apt update
apt install -y apache2 php snapd
snap install core
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/${DOMAIN}.conf
sed -i "9 s/#ServerName www.example.com/ServerName ${DOMAIN}/" /etc/apache2/sites-available/${DOMAIN}.conf
sed -i "10 a $(echo '\\t')ServerAlias www.${DOMAIN}" /etc/apache2/sites-available/${DOMAIN}.conf
a2dissite 000-default.conf
a2ensite ${DOMAIN}.conf
systemctl restart apache2
certbot -n --apache -d ${DOMAIN} -d www.${DOMAIN} -m admin@${DOMAIN} --agree-tos
useradd admin && $(echo admin:${PASSWORD} |chpasswd)
usermod -aG sudo admin
sed -i "16,17 s/www-data/admin/" /etc/apache2/envvars
chown -R admin:admin /var/www/html/
systemctl reload apache2.service
echo 'Работа скрипта завершена'
