#!/bin/bash

# -p PASSWORD

while getopts "p:" option; do
   case $option in
      p)
        PASSWORD=$OPTARG;;
     \?)
         echo "Error: Invalid option"
         exit;;
   esac
done

if [ -z "$PASSWORD" ] ; then
    echo "Usage: $0 [-p PASSWORD]";
    echo "Example: $0 -p password";
    exit 1;
fi

apt update
apt upgrade
useradd admin && $(echo admin:${PASSWORD} |chpasswd)
usermod -aG sudo admin
echo 'Работа скрипта завершена'
